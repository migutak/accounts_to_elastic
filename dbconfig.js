module.exports = {
    user          : process.env.DB_USER || "ecol",
    password      : process.env.DB_PASSWORD || 'ecol',
    connectString : process.env.DB_CONNECTIONSTRING || "127.0.0.1:1564/ORCLCDB.localdomain",
    elasticsearch : process.env.ELASTICSEARCH || 'http://localhost:9200/',
    partable : process.env.PARTABLE || 'PAR28FEB2022',
    parbranch : process.env.PARBRANCH || 'PAR21MAR2022',
    parhomogeneous : process.env.PARHOMEGENEOUS || 'PAR21MAR2022HOM',
  };
  