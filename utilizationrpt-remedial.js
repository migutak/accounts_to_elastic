var oracledb = require('oracledb');
var dbConfig = require('./dbconfig.js');
var elasticsearch = require('elasticsearch');
const dbconfig = require('./dbconfig.js');

var client = new elasticsearch.Client({
    hosts: [
        dbconfig.elasticsearch
    ]
});

var rowcount = 0;

oracledb.getConnection(
    {
        user: dbConfig.user,
        password: dbConfig.password,
        connectString: dbConfig.connectString
    },
    function (err, connection) {
        if (err) {
            console.error(err.message);
            return;
        }
        //
        var stream = connection.queryStream(
            "select * from tbl_utilization_remedial a where rowid in (select min(rowid) from tbl_utilization_remedial b where b.custnumber=a.custnumber)",
            [],  // no binds
            { fetchArraySize: 150, outFormat: oracledb.OBJECT }  // internal buffer size for performance tuning

        );
        stream.on('error', function (error) {
            // console.log("stream 'error' event");
            console.error(error);
            return;
        });

        stream.on('metadata', function (metadata) {
            // console.log("stream 'metadata' event");
            //console.log(metadata);
        });

        stream.on('data', function (data) {
            // console.log("stream 'data' event");
            //
            console.log(data)
            run(data)
            rowcount++;
        });

        stream.on('end', function () {
            // console.log("stream 'end' event");
            console.log('Rows selected: ' + rowcount);
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        });
    });

async function run(data) {
    await client.index({
        index: 'utilizationrpt-remedial',
        //id: data.ACCNUMBER,
        body: data
    })

}