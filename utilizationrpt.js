var oracledb = require('oracledb');
var dbConfig = require('./dbconfig.js');
var elasticsearch = require('elasticsearch');
const dbconfig = require('./dbconfig.js');

var client = new elasticsearch.Client({
    hosts: [
        dbconfig.elasticsearch
    ]
});

var rowcount = 0;

oracledb.getConnection(
    {
        user: dbConfig.user,
        password: dbConfig.password,
        connectString: dbConfig.connectString
    },
    function (err, connection) {
        if (err) {
            console.error(err.message);
            return;
        }

        var stream = connection.queryStream(
            "SELECT TO_CHAR(SYSDATE, 'DD-MON-YYYY') reportdate,accnumber,custnumber,client_name,t.arocode,t.arocode||' '||t.aroname aroname,rrocode,branchcode,branchname,region,oustbalance,totalarrears,"+
            "daysinarr,CASE  WHEN daysinarr>360 THEN '360+' WHEN daysinarr>180 THEN '180+' WHEN daysinarr>120 THEN '120+' WHEN daysinarr>90 THEN '90+' WHEN daysinarr>60 THEN '60+' WHEN daysinarr>30 THEN '30+' WHEN daysinarr>7 THEN '7+' else '1+' end as bucket,productcode,lastnotemade(accnumber) lastnotemade, ifnoteexists(accnumber) Noofnoteslast30days, "+
            "case ifnoteexists(accnumber) when '0' then 1 else 0 end nonactioned,case ifnoteexists(accnumber) when '0' then 0 else 1 end actioned "+
            "FROM tqall t  where substr(accnumber,0,3) = '016' and rrocode='RRO00' and substr(accnumber,3,2) not in ('6M') and arocode not in ('HRD')",
            [],  // no binds
            { fetchArraySize: 150, outFormat: oracledb.OBJECT }  // internal buffer size for performance tuning

        );
        stream.on('error', function (error) {
            // console.log("stream 'error' event");
            console.error(error);
            return;
        });

        stream.on('metadata', function (metadata) {
            // console.log("stream 'metadata' event");
            //console.log(metadata);
        });

        stream.on('data', function (data) {
            // console.log("stream 'data' event");
            //
            console.log(data)
            run(data)
            rowcount++;
        });

        stream.on('end', function () {
            // console.log("stream 'end' event");
            console.log('Rows selected: ' + rowcount);
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        });
    });

async function run(data) {
    try {
        await client.index({
            index: 'utilizationrpt_branch',
            //id: data.ACCNUMBER,
            body: data
        })
    } catch (error) {
        console.log(error)
    }
    

}