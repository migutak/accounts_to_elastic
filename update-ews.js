var elasticsearch = require('elasticsearch');
var client = new elasticsearch.Client({
    hosts: [
        process.env.ELASTICSEARCH || 'http://localhost:9200/'
    ]
});

var express = require("express");
var cors = require('cors');
var app = express();

const corsConfig = {
    credentials: true,
    origin: true,
};

app.use(cors(corsConfig));
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.get('/elasticsearchupdate/ews_on_elasticsearch/update', function (req, res) {
    res.status(200).json({
        status: 'OK'
    });
});

app.post('/elasticsearchupdate/clientactionplan_on_elasticsearch/getid/:id', function (req, res) {
    var resource = req.body;
    var param = {
        index: 'test', type: 'doc', _id: req.params.id, doc: resource
    };

    client.update(param, function (error, response) {
        //   client.get({ index: 'test', type: 'tes', id: req.params.id }, function(err, resource) {
        res.send(response);
    });
    //  });
});

app.post("/elasticsearchupdate/ews_on_elasticsearch/update", function (request, response) {

    async function run() {
        try {
            await client.update({
                index: request.body.index,
                id: request.body.CD_ACCOUNT,
                body: {
                    doc: request.body
                }
            })

            const { body } = await client.get({
                index: request.body.index,
                id: request.body.CD_ACCOUNT
            })

            console.log(body)

            response.status(200).json({
                status: 'OK',
                data: body
            });
        } catch (error) {
            console.error(error);
            response.status(500).json({
                status: 'fail',
                error: error
            });
        }
    }

    run()
})

app.post("/elasticsearchupdate/clientactionplan_on_elasticsearch/update", function (request, response) {

    client.get({ index: request.body.index_es, id: request.body.custnumber }, function (err, resource) {
        
        if (err) { 
            
            console.log(err.status) 
            //post this can also work as update
            runpost();
        
        } else {
            //
            runpost();
        }
    })

    async function runpost() {
        try {
            await client.index({
                index: request.body.index_es,
                id: request.body.custnumber,
                body:  request.body
            })

           const { body } = await client.get({
                index: request.body.index_es,
                id: request.body.custnumber
            })

            console.log(body)

            response.status(200).json({
                status: 'OK',
               data: body
            });
        } catch (error) {
            console.error(error);
            response.status(500).json({
                status: 'fail',
                error: error
            });
        }
    }

})

var server = app.listen(process.env.PORT || 5401, function () {
    console.log("Listening on port %s...", server.address().port);
});
