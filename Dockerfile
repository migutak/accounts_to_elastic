FROM oraclelinux:8-slim

LABEL "provider"="Oracle"                                               \
  "issues"="https://github.com/oracle/docker-images/issues"

ARG release=19
ARG update=9

RUN  microdnf install oracle-release-el8 && \
  microdnf install oracle-instantclient${release}.${update}-basic oracle-instantclient${release}.${update}-devel oracle-instantclient${release}.${update}-sqlplus && \
  microdnf install nodejs &&\
  microdnf install iputils telnet -y &&\
  microdnf clean all

RUN useradd -ms /bin/bash node
USER node
RUN mkdir -p /home/node/app && chmod 777 /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node package*.json ./

RUN npm install

# Bundle app source code

COPY --chown=node:node . .

# Bind to all network interfaces so that it can be mapped to the host OS
ENV HOST=0.0.0.0 PORT=5401

EXPOSE ${PORT}
CMD [ "node", "update-ews" ]
# docker build -t migutak/accounts_to_elastic:5.7.5 .
