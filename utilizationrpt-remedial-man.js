var oracledb = require('oracledb');
var dbConfig = require('./dbconfig.js');
var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
    hosts: [
        dbConfig.elasticsearch
    ]
});

var rowcount = 0;

oracledb.getConnection(
    {
        user: dbConfig.user,
        password: dbConfig.password,
        connectString: dbConfig.connectString
    },
    function (err, connection) {
        if (err) {
            console.error(err.message);
            return;
        }
        //
        var stream = connection.queryStream(
            //"select * from tbl_utilization_remedial_man",
            "SELECT to_char(reportdate,'DD-MON-YYYY') pardate,customercode,loan_account,accountname,t.arocode,t.aroname,t.rrocode,t.rrocode||' '||t.rroname rroname,t.branch,region,"+
            "analysedcategory,memo,lastnotemade_remedial(customercode, u.remedialunit) lastnotemade,u.remedialunit, totaldebt,business_unit, "+
            "case ifnoteexists_remedial(customercode, u.remedialunit) when '0' then 1 else 0 end nonactioned,case ifnoteexists_remedial(customercode, u.remedialunit) when '0' then 0 else 1 end actioned,"+
            "case ifnoteexists_remedial(customercode, u.remedialunit) when '0' then 'N' else 'Y' end utilization, "+
            "current_timestamp lastupdate, customercode||'_'||reportdate id "+
            "FROM "+dbConfig.partable+" t left join tblusers u on t.rrocode=u.rrocode",
            [],  // no binds
            { fetchArraySize: 150, outFormat: oracledb.OBJECT }  // internal buffer size for performance tuning

        );
        stream.on('error', function (error) {
            // console.log("stream 'error' event");
            console.error(error);
            return;
        });

        stream.on('metadata', function (metadata) {
            // console.log("stream 'metadata' event");
            //console.log(metadata);
        });

        stream.on('data', function (data) {
            // console.log("stream 'data' event");
            //
            console.log(data)
            run(data)
            rowcount++;
        });

        stream.on('end', function () {
            // console.log("stream 'end' event");
            console.log('Rows selected: ' + rowcount);
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        });
    });

async function run(data) {
    await client.index({
        index: 'utilizationrpt-remedial-man',
        id: data.ID,
        body: data
    })

}