var oracledb = require('oracledb');
var dbConfig = require('./dbconfig.js');
var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
    hosts: [
        dbConfig.elasticsearch
    ]
});

var rowcount = 0;

oracledb.getConnection(
    {
        user: dbConfig.user,
        password: dbConfig.password,
        connectString: dbConfig.connectString
    },
    function (err, connection) {
        if (err) {
            console.error(err.message);
            return;
        }
        //
        var stream = connection.queryStream(
            "SELECT to_char(sysdate,'DD-MON-YYYY') pardate,custnumber customer_code,accnumber loan_account,client_name accountname,t.arocode,t.arocode||' ' ||t.aroname aroname,t.branchname,region,"+
            "daysinarr, CASE WHEN daysinarr>360 THEN '360+' WHEN daysinarr>180 THEN '180+' WHEN daysinarr>120 THEN '120+' WHEN daysinarr>90 THEN '90+' WHEN daysinarr>60 THEN '60+' WHEN daysinarr>30 THEN '30+' WHEN daysinarr>7 THEN '7+' WHEN daysinarr is null THEN '90+' else '1+' end as analysedcategory,substr(accnumber,3,3) memo,lastnotemade_branch(accnumber, u.branch) lastnotemade,u.remedialunit, oustbalance totaldebt,productcode business_unit, "+
            "case ifnoteexists_branch(accnumber, u.branch) when '0' then 1 else 0 end nonactioned,case ifnoteexists_branch(accnumber, u.branch) when '0' then 0 else 1 end actioned,"+
            "case ifnoteexists_branch(accnumber, u.branch) when '0' then 'N' else 'Y' end utilization,"+ 
            "current_timestamp lastupdate, accnumber||'_'||to_char(sysdate,'MON-YYYY') id "+
            "FROM tqall t left join tblusers u on t.arocode=u.arocode where  substr(accnumber,1,3) = '016' and t.rrocode='RRO00' and t.branchname not in ('TREASURY BRANCH','TRANSMASTER') and substr(t.arocode,1,4) not in ('CORP', 'SACO', 'DIAS')",

            /*"SELECT to_char(reportdate,'DD-MON-YYYY') pardate,customer_code,loan_account,accountname,t.arocode,t.arocode||' ' ||t.aroname,t.rro_code,t.rro_name ,t.branch,region,"+
            "analysedcategory,memo,lastnotemade_branch(loan_account, u.branch) lastnotemade,u.remedialunit, totaldebt,business_unit, "+
            "case ifnoteexists_branch(loan_account, u.branch) when '0' then 1 else 0 end nonactioned,case ifnoteexists_branch(loan_account, u.branch) when '0' then 0 else 1 end actioned,"+
            "case ifnoteexists_branch(loan_account, u.branch) when '0' then 'N' else 'Y' end utilization,"+ 
            "current_timestamp lastupdate, loan_account||'_'||reportdate id "+
            "FROM "+dbConfig.parbranch+" t left join tblusers u on t.arocode=u.arocode where t.branch not in ('TREASURY BRANCH','TRANSMASTER') and substr(t.arocode,1,4) not in ('CORP', 'SACO', 'DIAS')",*/
            [],  // no binds
            { fetchArraySize: 150, outFormat: oracledb.OBJECT }  // internal buffer size for performance tuning

        );
        stream.on('error', function (error) {
            // console.log("stream 'error' event");
            console.error(error);
            return;
        });

        stream.on('metadata', function (metadata) {
            // console.log("stream 'metadata' event");
            //console.log(metadata);
        });

        stream.on('data', function (data) {
            // console.log("stream 'data' event");
            //
            console.log(data)
            run(data)
            rowcount++;
        });

        stream.on('end', function () {
            // console.log("stream 'end' event");
            console.log('Rows selected: ' + rowcount);
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        });
    });

async function run(data) {
    try {
        await client.index({
            index: 'utilizationrpt-branch',
            id: data.ID,
            body: data
        })
    } catch (error) {
        console.log(error)
    }
    

}