var oracledb = require('oracledb');
var dbConfig = require('./dbconfig.js');
const { Client } = require('@elastic/elasticsearch')
var elasticsearch = require('elasticsearch');
//const client = new Client({ node: process.env.ELASTICSEARCH || 'http://localhost:9200' })
var client = new elasticsearch.Client({
    hosts: [
        process.env.ELASTICSEARCH || 'http://localhost:9200/'
    ]
});

var rowcount = 0;

oracledb.getConnection(
    {
        user: dbConfig.user,
        password: dbConfig.password,
        connectString: dbConfig.connectString
    },
    function (err, connection) {
        if (err) {
            console.error(err.message);
            return;
        }

        var stream = connection.queryStream(
            'SELECT * FROM TBL_EARLYWARNINGSIGN t left join TBL_EARLYWARNINGSIGN_STATIC s on t.cd_account=s.accnumber',
            [],  // no binds
            { fetchArraySize: 150, outFormat: oracledb.OBJECT }  // internal buffer size for performance tuning

        );

        stream.on('error', function (error) {
            // console.log("stream 'error' event");
            console.error(error);
            return;
        });

        stream.on('metadata', function (metadata) {
            // console.log("stream 'metadata' event");
            //console.log(metadata);
        });

        stream.on('data', function (data) {
            // console.log("stream 'data' event");
            //sene to ews queue
            run(data)
            rowcount++;
        });

        stream.on('end', function () {
            // console.log("stream 'end' event");
            console.log('Rows selected: ' + rowcount);
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        });
    });

async function run(data) {
    await client.index({
        index: 'ews',
        id: data.CD_ACCOUNT,
        body: data
    })

    const { body } = await client.get({
        index: 'ews',
        id: data.CD_ACCOUNT
    })

    console.log('added', body)
}